<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TypeDiscount extends Enum
{
    const PERCENT = '%';
    const CASH = 'cash';
}

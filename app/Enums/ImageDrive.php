<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ImageDrive extends Enum
{
    const DEFAULT = 'local';
    const S3 = 's3';
}

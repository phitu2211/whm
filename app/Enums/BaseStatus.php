<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class BaseStatus extends Enum
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const OPTIONS = [
        self::ACTIVE => 'Hoạt Động',
        self::INACTIVE => 'Đóng',
    ];
    const KEYS = [self::ACTIVE, self::INACTIVE];
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TypeUpdateQuantity extends Enum
{
    const INCREASE = '+';
    const DECREASE = '-';
}

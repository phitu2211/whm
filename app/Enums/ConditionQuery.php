<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ConditionQuery extends Enum
{
    const LIKE = 'like';
}

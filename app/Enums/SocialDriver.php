<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SocialDriver extends Enum
{
    const FB = 'facebook';
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class TypeTax extends Enum
{
    const BY_PRODUCT = 'tax_by_product';
    const BY_ORDER = 'tax_by_order';
    const TYPE_TAX_OPTIONS = [
        self::BY_ORDER => 'Thuế theo đơn hàng',
        self::BY_PRODUCT => 'Thuế theo sản phẩm',
    ];
}

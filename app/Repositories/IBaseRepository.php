<?php

namespace App\Repositories;

interface IBaseRepository
{
    public function getModel();

    public function getBuilder();

    public function setModel($model);

    public function all();

    public function paginate(int $perPage = 15);

    public function filter(array $param);

    public function onlyParent(string $column = 'parent_id');

    public function create(array $data = []);

    public function update(int $id, array $data = [], $is_object = true);

    public function delete(int $id);

    public function find(int $id);

    public function findWhere(array $attributes);

    public function firstWhere(array $attributes);

    public function firstByField($field, $value, string $operator = '=');
}

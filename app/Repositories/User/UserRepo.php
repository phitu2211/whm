<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;

class UserRepo extends BaseRepository implements IUserRepo
{
    public function getUserByEmail(string $email)
    {
        return $this->getModel()->whereEmail($email)->first();
    }
}

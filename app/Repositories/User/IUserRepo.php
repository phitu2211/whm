<?php

namespace App\Repositories\User;

use App\Repositories\IBaseRepository;

interface IUserRepo extends IBaseRepository
{
    public function getUserByEmail(string $email);
}

<?php

namespace App\Repositories\Product;

use App\Repositories\IBaseRepository;

interface IProductRepo extends IBaseRepository
{
    function updateQuantity(int $quantity);
}

<?php

namespace App\Repositories\Product;

use App\Repositories\BaseRepository;

class ProductRepo extends BaseRepository implements IProductRepo
{
    public function updateQuantity(int $quantity)
    {
        return $this->update($this->model->id, [
            'quantity' => $quantity
        ]);
    }
}

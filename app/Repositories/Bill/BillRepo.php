<?php

namespace App\Repositories\Bill;

use App\Repositories\BaseRepository;

class BillRepo extends BaseRepository implements IBillRepo
{
    public function findByCode(string $code)
    {
        return $this->firstWhere(['code', '=', $code]);
    }
}

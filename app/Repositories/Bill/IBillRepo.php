<?php

namespace App\Repositories\Bill;

use App\Repositories\IBaseRepository;

interface IBillRepo extends IBaseRepository
{
    public function findByCode(string $code);
}

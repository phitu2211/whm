<?php

namespace App\Repositories\Order;

use App\Models\Product;
use App\Repositories\IBaseRepository;

interface IOrderRepo extends IBaseRepository
{
    public function updateQuantityProductInOrder(Product $product, int $quantity);

    public function findOrCreateProduct(Product $product);

    public function updateTotalAmount();
}

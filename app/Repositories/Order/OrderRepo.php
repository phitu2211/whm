<?php

namespace App\Repositories\Order;

use App\Enums\TypeDiscount;
use App\Models\Product;
use App\Repositories\BaseRepository;

class OrderRepo extends BaseRepository implements IOrderRepo
{
    public function updateQuantityProductInOrder(Product $product, int $quantity)
    {
        $orderProduct = $product->pivot;
        $orderProduct->quantity = $quantity;
        $orderProduct->save();
        $this->updateTotalAmount();
    }

    public function findOrCreateProduct(Product $product)
    {
        $order = $this->model;

        $productInOrder = $order->products()->wherePivot('product_id', $product->id)->first();
        if (!$productInOrder) {
            $productInOrder = $order->products()->save($product, [
                'quantity' => 0
            ]);
            $productInOrder = $order->products()->find($product->id);
        }

        return $productInOrder;
    }

    public function updateTotalAmount()
    {
        $order = $this->model;

        return $this->update($order->id, [
            'total_amount' => $order->products_amount - $order->discount_amount  + $order->tax_amount
        ]);
    }
}

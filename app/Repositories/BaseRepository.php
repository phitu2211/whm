<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository implements IBaseRepository
{
    protected $model;
    protected $original_model;
    protected $builder;

    public function __construct(Model $model)
    {
        $this->original_model = $model;
        $this->model = $model;
        $this->builder = $model->query();
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function all()
    {
        return $this->builder->get();
    }

    public function paginate($perPage = 15)
    {
        return $this->builder->paginate();
    }

    public function filter(array $param)
    {
        $this->builder = $this->builder->filter($param);
        return $this;
    }

    public function onlyParent(string $column = 'parent_id')
    {
        $this->builder = $this->builder->onlyParent($column);
        return $this;
    }

    public function create($data = [])
    {
        return $this->original_model->create($data)->fresh();
    }

    public function update($id, $data = [], $is_object = true)
    {
        $record = $this->original_model->findOrFail($id);

        return $is_object ? tap($record)->update($data)->fresh() : $record->update($data)->fresh();
    }

    public function delete($id)
    {
        return $this->original_model->destroy($id);
    }

    public function find($id)
    {
        return $this->original_model->findOrFail($id);
    }

    public function findWhere(array $attributes)
    {
        foreach ($attributes as $attribute) {
            list($field, $operator, $value) = $attribute;
            $this->builder = $this->builder->where($field, $operator, $value);
        }
        return $this->builder->get();
    }

    public function firstWhere(array $attributes)
    {
        foreach ($attributes as $attribute) {
            list($field, $operator, $value) = $attribute;
            $this->builder = $this->builder->where($field, $operator, $value);
        }
        return $this->builder->first();
    }

    public function firstByField($field, $value, string $operator = '=')
    {
        return $this->builder->where($field, $operator, $value)->first();
    }
}

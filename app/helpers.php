<?php

use App\Enums\ImageDrive;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

if (!function_exists('avatar_shop')) {
    function avatar_shop()
    {
        return asset('uploads/avatar_shop.jpg');
    }
}

if (!function_exists('invalidate_token')) {
    function invalidate_token()
    {
        if (JWTAuth::check()) JWTAuth::invalidate();
    }
}

if (!function_exists('image_by_drive')) {
    function image_by_drive($drive, $path)
    {
        $image = avatar_shop();
        switch ($drive) {
            case ImageDrive::S3:
                $image = Storage::disk(ImageDrive::S3)->url($path);
                break;
            default:
                $image = asset($path);
                break;
        }
        return $image;
    }
}

if (!function_exists('gen_file_name')) {
    function gen_file_name()
    {
        return time() . "_" . mt_rand(1, 1000);
    }
}

if (!function_exists('gen_file_name_with_extension')) {
    function gen_file_name_with_extension($extension)
    {
        return time() . "_" . mt_rand(1, 1000) . ".$extension";
    }
}

if (!function_exists('rule_in')) {
    function rule_in(string $enum)
    {
        return implode(',', $enum::getValues());
    }
}

<?php

namespace App\Models;

use App\Enums\BaseStatus;
use App\Traits\Filterable;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use HasFactory, Filterable, SerializeDateTime;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'address',
        'website',
        'status'
    ];

    protected $filterable = [
        'name' => 'like',
        'email' => 'like',
        'phone' => '=',
        'address' => 'like',
        'status' => '='
    ];

    public function getStatusTextAttribute()
    {
        return BaseStatus::OPTIONS[$this->status];
    }

    public function getAvatarAttribute()
    {
        if (!$this->media)
            return avatar_shop();

        return $this->media->image;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_has_shops', 'shop_id', 'user_id');
    }

    public function media()
    {
        return $this->morphOne(Media::class, 'mediable');
    }
}

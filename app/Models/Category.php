<?php

namespace App\Models;

use App\Enums\BaseStatus;
use App\Traits\Filterable;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, SerializeDateTime, Filterable;

    protected $fillable = [
        'name',
        'parent_id',
        'shop_id',
        'status'
    ];

    protected $filterable = [
        'name' => 'like',
        'parent_id' => '=',
        'shop_id' => '=',
        'status' => '='
    ];

    public function isParent()
    {
        return empty($this->parent_id);
    }

    public function getStatusTextAttribute()
    {
        return BaseStatus::OPTIONS[$this->status];
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
}

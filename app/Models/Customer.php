<?php

namespace App\Models;

use App\Traits\Filterable;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory, Filterable, SerializeDateTime;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'name',
        'email',
        'address',
        'phone',
        'note',
    ];

    protected $filterable = [
        'name' => 'like',
        'email' => 'like',
        'address' => 'like',
        'phone' => '=',
        'shop_id' => '='
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable = [
        'drive',
        'path',
        'mediable_id',
        'mediable_type',
    ];

    public function getImageAttribute()
    {
        return image_by_drive($this->drive, $this->path);
    }

    public function mediable()
    {
        return $this->morphTo();
    }
}

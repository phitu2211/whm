<?php

namespace App\Models;

use App\Enums\BaseStatus;
use App\Enums\TypeDiscount;
use App\Traits\Filterable;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, Filterable, SerializeDateTime;

    protected $fillable = [
        'name',
        'barcode',
        'original_price',
        'discount',
        'type_discount',
        'tax',
        'final_price',
        'quantity',
        'favorite',
        'note',
        'sell_count',
        'status',
        'unit_id',
        'category_id',
        'shop_id',
    ];

    protected $filterable = [
        'name' => 'like',
        'barcode' => '=',
        'status' => '=',
        'favorite' => '=',
        'unit_id' => '=',
        'category_id' => '=',
        'shop_id' => '=',
    ];

    protected $casts = [
        'original_price' => 'float',
        'final_price' => 'float',
        'favorite' => 'boolean'
    ];

    public function getImageAttribute()
    {
        if (!$this->media)
            return avatar_shop();

        return $this->media->image;
    }

    public function getStatusTextAttribute()
    {
        return BaseStatus::OPTIONS[$this->status];
    }

    public function getTaxAmountAttribute()
    {
        return $this->final_price * ($this->tax / 100);
    }

    public function getDiscountAmountAttribute()
    {
        switch ($this->type_discount) {
            case TypeDiscount::PERCENT:
                $discount_amount = $this->final_price * ($this->discount / 100);
                break;
            default:
                $discount_amount = $this->discount;
                break;
        }
        return $discount_amount;
    }

    public function media()
    {
        return $this->morphOne(Media::class, 'mediable');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function hasStock($quantity = 1)
    {
        return $this->quantity >= $quantity;
    }
}

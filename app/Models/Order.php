<?php

namespace App\Models;

use App\Enums\TypeDiscount;
use App\Enums\TypeTax;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SerializeDateTime, SoftDeletes;

    protected $fillable = [
        'shop_id',
        'user_id',
        'customer_id',
        'code',
        'discount',
        'type_discount',
        'tax',
        'type_tax',
        'total_amount',
    ];

    protected $casts = [
        'tax' => 'float',
        'discount' => 'float',
        'total_amount' => 'float',
    ];

    public function getProductsAmountAttribute()
    {
        if (!$this->products)
            return 0;

        return $this->products->sum(function ($product) {
            return $product->pivot->quantity * ($product->final_price - $product->discount_amount);
        });
    }

    public function getDiscountAmountAttribute()
    {
        switch ($this->type_discount) {
            case TypeDiscount::PERCENT:
                $discount_amount = $this->products_amount * ($this->discount / 100);
                break;
            default:
                $discount_amount = $this->discount;
                break;
        }
        return $discount_amount;
    }

    public function getTaxAmountAttribute()
    {
        switch ($this->type_tax) {
            case TypeTax::BY_PRODUCT:
                $tax_amount = $this->tax_by_product;
                break;
            case TypeTax::BY_ORDER:
                $tax_amount = $this->tax_by_order;
                break;
            default:
                $tax_amount = $this->tax;
                break;
        }
        return $tax_amount;
    }

    public function getTaxByProductAttribute()
    {
        if (!$this->products)
            return 0;

        return $this->products->sum(function ($product) {
            return $product->pivot->quantity * $product->tax_amount;
        });
    }

    public function getTaxByOrderAttribute()
    {
        return $this->products_amount * ($this->tax / 100);
    }

    public function getTotalAmountByProductAttribute()
    {
        if (!$this->products)
            return 0;

        $total_tax = $this->products->sum(function ($product) {
            return $product->pivot->quantity * $product->tax_amount;
        });
        return $this->products_amount - $this->discount_amount + $total_tax;
    }

    public function getTotalAmountByOrderAttribute()
    {
        $total_tax = $this->products_amount * ($this->tax / 100);
        return $this->products_amount - $this->discount_amount + $total_tax;
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_has_products', 'order_id', 'product_id')->withPivot('quantity')->using(OrderHasProduct::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

<?php

namespace App\Models;

use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory, SerializeDateTime;

    protected $fillable = [
        'customer_id',
        'shop_id',
        'code',
        'discount_amount',
        'tax_amount',
        'customer_pay',
        'repay_amount',
        'total_amount',
    ];

    protected $casts = [
        'tax_amount' => 'float',
        'discount_amount' => 'float',
        'customer_pay' => 'float',
        'repay_amount' => 'float',
        'total_amount' => 'float',
        'created_at' => 'string',
        'updated_at' => 'string',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'bill_has_products', 'bill_id', 'product_id')->withPivot('quantity')->using(BillHasProduct::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}

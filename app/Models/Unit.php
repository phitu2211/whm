<?php

namespace App\Models;

use App\Traits\Filterable;
use App\Traits\SerializeDateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory, SerializeDateTime, Filterable;

    protected $fillable = [
        'name',
        'shop_id'
    ];

    protected $filterable = [
        'name' => 'like',
        'shop_id' => '='
    ];
}

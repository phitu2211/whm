<?php

namespace App\Traits;

use App\Enums\ConditionQuery;
use Illuminate\Support\Str;

trait Filterable
{
    public function scopeFilter($query, $param)
    {
        foreach ($param as $field => $value) {
            $method = 'filter' . Str::studly($field);

            if ($value === '' || $value === null) {
                continue;
            }

            if (method_exists($this, $method)) {
                $this->{$method}($query, $value);
                continue;
            }

            if (empty($this->filterable) || !is_array($this->filterable)) {
                continue;
            }

            if (key_exists($field, $this->filterable)) {
                $condition = $this->filterable[$field];
                if ($condition == ConditionQuery::LIKE)
                    $query->whereLike($this->table . '.' . $field, $value);
                else
                    $query->where($this->table . '.' . $field, $value);
                continue;
            }

            // if (key_exists($field, $this->filterable)) {
            //     $query->where($this->table . '.' . $this->filterable[$field], $value);
            //     continue;
            // }
        }

        return $query;
    }
}

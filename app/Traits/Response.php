<?php

namespace App\Traits;

use Throwable;

trait Response
{
    protected $data = [];

    protected $headers = [];

    protected $statusCode = 200;

    protected $message;

    protected $code;

    protected $exception;

    public function addData(array $data)
    {
        if (!key_exists('data', $data))
            $data = ['data' => $data];

        $this->data = $data;

        return $this;
    }

    public function setException(Throwable $exception)
    {
        $this->exception = $exception;
        return $this;
    }

    public function setHeaders(array $headers)
    {
        foreach ($headers as $key => $value) {
            $this->headers[$key] = $value;
        }

        return $this;
    }

    public function setStatusCode(int $code)
    {
        $this->statusCode = $code;

        return $this;
    }

    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    public function createSuccess()
    {
        $this->message = $this->msgSuccess['create'];
        return $this;
    }

    public function deleteSuccess()
    {
        $this->message = $this->msgSuccess['delete'];
        return $this;
    }

    public function updateSuccess()
    {
        $this->message = $this->msgSuccess['update'];
        return $this;
    }

    public function getResponse()
    {
        $data = [
            'error' => !$this->statusCode == 200 || !is_null($this->code),
            'message' => $this->message,
            'code' => $this->code
        ];

        $data = array_merge($data, $this->data);

        if ($exception = $this->exception) {
            $data['error'] = true;
            $data['code'] = $exception->getCode();
            $this->statusCode = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : $exception->getCode();

            $data['message'] = $this->statusCode == 500 ? "Đã có lỗi xảy ra! Vui lòng thử lại!" : $exception->getMessage();

            if (config('app.debug') && $this->statusCode != 200)
                $data['debug'] = $exception->getTrace();
        }

        return response()->json($data, $this->statusCode, $this->headers);
    }
}

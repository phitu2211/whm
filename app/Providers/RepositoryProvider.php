<?php

namespace App\Providers;

use App\Models\Bill;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Media;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Unit;
use App\Models\User;
use App\Repositories\Bill\BillRepo;
use App\Repositories\Bill\IBillRepo;
use App\Repositories\Category\CategoryRepo;
use App\Repositories\Category\ICategoryRepo;
use App\Repositories\Customer\CustomerRepo;
use App\Repositories\Customer\ICustomerRepo;
use App\Repositories\Media\IMediaRepo;
use App\Repositories\Media\MediaRepo;
use App\Repositories\Order\IOrderRepo;
use App\Repositories\Order\OrderRepo;
use App\Repositories\Product\IProductRepo;
use App\Repositories\Product\ProductRepo;
use App\Repositories\Shop\IShopRepo;
use App\Repositories\Shop\ShopRepo;
use App\Repositories\Unit\IUnitRepo;
use App\Repositories\Unit\UnitRepo;
use App\Repositories\User\IUserRepo;
use App\Repositories\User\UserRepo;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IUnitRepo::class, function () {
            return new UnitRepo(new Unit());
        });
        $this->app->bind(IShopRepo::class, function () {
            return new ShopRepo(new Shop());
        });
        $this->app->bind(ICustomerRepo::class, function () {
            return new CustomerRepo(new Customer());
        });
        $this->app->bind(IProductRepo::class, function () {
            return new ProductRepo(new Product());
        });
        $this->app->bind(ICategoryRepo::class, function () {
            return new CategoryRepo(new Category());
        });
        $this->app->bind(IMediaRepo::class, function () {
            return new MediaRepo(new Media());
        });
        $this->app->bind(IUserRepo::class, function () {
            return new UserRepo(new User());
        });
        $this->app->bind(IOrderRepo::class, function () {
            return new OrderRepo(new Order());
        });
        $this->app->bind(IBillRepo::class, function () {
            return new BillRepo(new Bill());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

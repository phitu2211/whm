<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MaxIf implements Rule
{
    private $callback;
    private $max;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($callback, $max)
    {
        $this->callback = $callback;
        $this->max = $max;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (call_user_func($this->callback) && $value > $this->max)
            return false;
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.max_if', ['max' => $this->max]);
    }
}

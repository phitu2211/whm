<?php

namespace App\Http\Controllers\V1;

use App\Enums\BaseStatus;
use App\Http\Requests\CategoryRequest;
use App\Repositories\Category\ICategoryRepo;
use App\Service\CategoryService;
use App\Transformers\CategoryTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class CategoryController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(ICategoryRepo $repo, CategoryService $service, CategoryTransformer $transformer)
    {
        $this->repo = $repo;
        $this->service = $service;
        $this->transformer = $transformer;
        $this->displayName = __('db.category');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();
        $categories = $this->repo->filter($param)->onlyParent()->all();
        $categories = Fractal::create()
            ->collection($categories, $this->transformer)
            ->toArray();

        return $this->addData($categories)->getResponse();
    }

    public function detail($id)
    {
        $category = $this->repo->find($id);
        $category = Fractal::create()
            ->item($category, $this->transformer)
            ->toArray();
        return $this->addData($category)->getResponse();
    }

    public function listStatus()
    {
        return $this->addData(BaseStatus::OPTIONS)->getResponse();
    }

    public function store(CategoryRequest $request)
    {
        $param = $request->all();
        $category = $this->repo->create($param);
        $category = Fractal::create()
            ->item($category, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($category)->getResponse();
    }

    public function update(CategoryRequest $request, $id)
    {
        $param = $request->all();
        $category = $this->repo->update($id, $param);
        $category = Fractal::create()
            ->item($category, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($category)->getResponse();
    }

    public function destroy($id)
    {
        $this->service->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

<?php

namespace App\Http\Controllers\V1;

use App\Traits\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Response;

    protected $displayName;

    protected $msgSuccess;

    public function boot()
    {
        $this->msgSuccess = [
            'create' => __('common.create_success', ['model' => $this->displayName]),
            'update' => __('common.update_success', ['model' => $this->displayName]),
            'delete' => __('common.delete_success', ['model' => $this->displayName]),
        ];
    }
}

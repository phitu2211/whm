<?php

namespace App\Http\Controllers\V1;

use App\Enums\BaseStatus;
use App\Http\Requests\ShopRequest;
use App\Repositories\Shop\IShopRepo;
use App\Repositories\Shop\ShopRepo;
use App\Service\ShopService;
use App\Transformers\ShopTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class ShopController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(IShopRepo $repo, ShopService $service, ShopTransformer $transformer)
    {
        $this->repo = $repo;
        $this->service = $service;
        $this->transformer = $transformer;
        $this->displayName = __('db.shop');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();

        if (auth()->user()->isAdmin()) {
            $shops = $this->repo->filter($param)->paginate();
        } else {
            $shops = auth()->user()->shops()->filter($param)->paginate();
        }

        $shops = Fractal::create()
            ->collection($shops->getCollection(), $this->transformer)
            ->paginateWith(new IlluminatePaginatorAdapter($shops))
            ->toArray();

        return $this->addData($shops)->getResponse();
    }

    public function listStatus()
    {
        return $this->addData(BaseStatus::OPTIONS)->getResponse();
    }

    public function detail($id)
    {
        $shop = $this->repo->find($id);
        $shop = Fractal::create()
            ->item($shop, $this->transformer)
            ->toArray();
        return $this->addData($shop)->getResponse();
    }

    public function store(ShopRequest $request)
    {
        $param = $request->all();
        $shop = $this->service->create($param);
        $shop = Fractal::create()
            ->item($shop, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($shop)->getResponse();
    }

    public function update(ShopRequest $request, $id)
    {
        $param = $request->all();
        $shop = $this->service->update($param, $id);
        $shop = Fractal::create()
            ->item($shop, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($shop)->getResponse();
    }

    public function destroy($id)
    {
        $this->service->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

<?php

namespace App\Http\Controllers\V1;

use App\Enums\BaseStatus;
use App\Http\Requests\ProductRequest;
use App\Repositories\Product\IProductRepo;
use App\Service\ProductService;
use App\Transformers\ProductTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class ProductController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(IProductRepo $repo, ProductService $service, ProductTransformer $transformer)
    {
        $this->repo = $repo;
        $this->service = $service;
        $this->transformer = $transformer;
        $this->displayName = __('db.product');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();
        $products = $this->repo->filter($param)->paginate();
        $products = Fractal::create()
            ->collection($products->getCollection(), $this->transformer)
            ->paginateWith(new IlluminatePaginatorAdapter($products))
            ->parseExcludes(['shop', 'category', 'unit'])
            ->toArray();

        return $this->addData($products)->getResponse();
    }

    public function listStatus()
    {
        return $this->addData(BaseStatus::OPTIONS)->getResponse();
    }

    public function detail($id)
    {
        $product = $this->repo->find($id);
        $product = Fractal::create()
            ->item($product, $this->transformer)
            ->parseIncludes(['shop', 'category', 'unit'])
            ->toArray();
        return $this->addData($product)->getResponse();
    }

    public function store(ProductRequest $request)
    {
        $param = $request->all();
        $product = $this->service->create($param);
        $product = Fractal::create()
            ->item($product, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($product)->getResponse();
    }

    public function update(ProductRequest $request, $id)
    {
        $param = $request->all();
        $product = $this->service->update($param, $id);
        $product = Fractal::create()
            ->item($product, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($product)->getResponse();
    }

    public function destroy($id)
    {
        $this->service->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\UnitRequest;
use App\Repositories\Unit\IUnitRepo;
use App\Transformers\UnitTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class UnitController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(IUnitRepo $repo, UnitTransformer $transformer)
    {
        $this->repo = $repo;
        $this->transformer = $transformer;
        $this->displayName = __('db.unit');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();
        $units = $this->repo->filter($param)->all();
        $units = Fractal::create()
            ->collection($units, $this->transformer)
            ->toArray();

        return $this->addData($units)->getResponse();
    }

    public function detail($id)
    {
        $unit = $this->repo->find($id);
        $unit = Fractal::create()
            ->item($unit, $this->transformer)
            ->toArray();
        return $this->addData($unit)->getResponse();
    }

    public function store(UnitRequest $request)
    {
        $param = $request->all();
        $unit = $this->repo->create($param);
        $unit = Fractal::create()
            ->item($unit, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($unit)->getResponse();
    }

    public function update(UnitRequest $request, $id)
    {
        $param = $request->all();
        $unit = $this->repo->update($id, $param);
        $unit = Fractal::create()
            ->item($unit, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($unit)->getResponse();
    }

    public function destroy($id)
    {
        $this->repo->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

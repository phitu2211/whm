<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\Order\OrderRequest;
use App\Http\Requests\Order\PaymentRequest;
use App\Http\Requests\Order\UpdateProductRequest;
use App\Http\Requests\Order\UpdateQuantityRequest;
use App\Repositories\Bill\IBillRepo;
use App\Repositories\Order\IOrderRepo;
use App\Service\BillService;
use App\Service\OrderService;
use App\Transformers\BillTransformer;
use App\Transformers\OrderTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class OrderController extends ApiController
{
    protected $repo;
    protected $billService;
    protected $service;
    protected $transformer;

    public function __construct(
        IOrderRepo $repo,
        OrderTransformer $transformer,
        OrderService $service,
        BillService $billService
    ) {
        $this->repo = $repo;
        $this->billService = $billService;
        $this->transformer = $transformer;
        $this->service = $service;
        $this->displayName = __('db.order');
        $this->boot();
    }

    public function index($shop_id)
    {
        $order = $this->service->getOrderByShopId($shop_id);

        $order = Fractal::create()
            ->item($order, $this->transformer)
            ->toArray();

        return $this->addData($order)->getResponse();
    }

    public function payment(PaymentRequest $request)
    {
        try {
            $order_id = $request->post('order_id');
            $customer_pay = $request->post('customer_pay');
            $order = $this->repo->find($order_id);

            $bill = $this->billService->createByOrder($order, $customer_pay);

            $bill = Fractal::create()
                ->item($bill, new BillTransformer)
                ->toArray();

            return $this->setMessage(__('translate.payment_success'))->addData($bill)->getResponse();
        } catch (\Throwable $th) {
            return $this->setException($th)->getResponse();
        }
    }

    public function update(OrderRequest $request, $order_id)
    {
        try {
            $param = $request->all();

            $order = $this->repo->update($order_id, $param);

            $order = Fractal::create()
                ->item($order, $this->transformer)
                ->toArray();

            return $this->updateSuccess()->addData($order)->getResponse();
        } catch (\Throwable $th) {
            return $this->setException($th)->getResponse();
        }
    }

    public function updateProduct(UpdateProductRequest $request)
    {
        try {
            $product_id = $request->post('product_id');
            $shop_id = $request->post('shop_id');
            $quantity = $request->post('quantity');
            $this->service->updateProductInOrder($product_id, $shop_id, $quantity);

            $order = $this->service->getOrderByShopId($shop_id);
            $order = Fractal::create()
                ->item($order, $this->transformer)
                ->toArray();

            return $this->updateSuccess()->addData($order)->getResponse();
        } catch (\Throwable $th) {
            return $this->setException($th)->getResponse();
        }
    }

    public function updateQuantity(UpdateQuantityRequest $request)
    {
        try {
            $product_id = $request->post('product_id');
            $shop_id = $request->post('shop_id');
            $quantity = $request->post('quantity');
            $type = $request->post('type');
            $this->service->setTypeUpdate($type)->setQuantityUpdate($quantity)->updateQuantityProductInOrder($product_id, $shop_id);

            $order = $this->service->getOrderByShopId($shop_id);
            $order = Fractal::create()
                ->item($order, $this->transformer)
                ->toArray();

            return $this->updateSuccess()->addData($order)->getResponse();
        } catch (\Throwable $th) {
            return $this->setException($th)->getResponse();
        }
    }

    public function destroy($id)
    {
        $this->repo->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

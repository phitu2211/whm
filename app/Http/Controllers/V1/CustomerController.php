<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\CustomerRequest;
use App\Repositories\Customer\ICustomerRepo;
use App\Transformers\CustomerTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class CustomerController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(ICustomerRepo $repo, CustomerTransformer $transformer)
    {
        $this->repo = $repo;
        $this->transformer = $transformer;
        $this->displayName = __('db.customer');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();
        $customers = $this->repo->filter($param)->paginate();
        $customers = Fractal::create()
            ->collection($customers->getCollection(), $this->transformer)
            ->paginateWith(new IlluminatePaginatorAdapter($customers))
            ->toArray();

        return $this->addData($customers)->getResponse();
    }

    public function detail($id)
    {
        $customer = $this->repo->find($id);
        $customer = Fractal::create()
            ->item($customer, $this->transformer)
            ->toArray();
        return $this->addData($customer)->getResponse();
    }

    public function store(CustomerRequest $request)
    {
        $param = $request->all();
        $customer = $this->repo->create($param);
        $customer = Fractal::create()
            ->item($customer, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($customer)->getResponse();
    }

    public function update(CustomerRequest $request, $id)
    {
        $param = $request->all();
        $customer = $this->repo->update($id, $param);
        $customer = Fractal::create()
            ->item($customer, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($customer)->getResponse();
    }

    public function destroy($id)
    {
        $this->repo->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

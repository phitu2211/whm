<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\UserRequest;
use App\Repositories\User\IUserRepo;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class UserController extends ApiController
{
    protected $repo;
    protected $service;
    protected $transformer;

    public function __construct(IUserRepo $repo, UserTransformer $transformer)
    {
        $this->repo = $repo;
        $this->transformer = $transformer;
        $this->displayName = __('db.user');
        $this->boot();
    }

    public function index(Request $request)
    {
        $param = $request->all();
        $users = $this->repo->filter($param)->all();
        $users = Fractal::create()
            ->collection($users, $this->transformer)
            ->toArray();

        return $this->addData($users)->getResponse();
    }

    public function detail($id)
    {
        $user = $this->repo->find($id);
        $user = Fractal::create()
            ->item($user, $this->transformer)
            ->toArray();
        return $this->addData($user)->getResponse();
    }

    public function store(UserRequest $request)
    {
        $param = $request->all();
        $user = $this->repo->create($param);
        $user = Fractal::create()
            ->item($user, $this->transformer)
            ->toArray();
        return $this->createSuccess()->addData($user)->getResponse();
    }

    public function update(UserRequest $request, $id)
    {
        $param = $request->all();
        $user = $this->repo->update($id, $param);
        $user = Fractal::create()
            ->item($user, $this->transformer)
            ->toArray();
        return $this->updateSuccess()->addData($user)->getResponse();
    }

    public function destroy($id)
    {
        $this->repo->delete($id);
        return $this->deleteSuccess()->getResponse();
    }
}

<?php

namespace App\Http\Controllers\V1;

use App\Http\Requests\UserRequest;
use App\Service\UserService;
use App\Transformers\UserTransformer;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class AuthController extends ApiController
{
    protected $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserRequest $request)
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            throw new AuthenticationException();
        }

        $this->service->updateToken($token);

        return $this->addData([
            'token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ])->getResponse();
    }

    public function loginFb(Request $request)
    {
        $request->validate([
            'token' => 'required',
        ]);
        $token = $request->post('token');

        $user = $this->service->createUserByTokenFb($token);
        $token = auth('api')->login($user);
        $this->service->updateToken($token);

        return $this->addData([
            'token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ])->getResponse();
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = Fractal::create()
            ->item(auth()->user(), app(UserTransformer::class))
            ->toArray();
        return $this->addData($user)->getResponse();
    }

    /**
     * Log the user out (Invalidate the token).
     */
    public function logout()
    {
        auth()->logout();

        return $this->setMessage('Successfully logged out')->getResponse();
    }

    /**
     * Refresh a token.
     */
    public function refresh()
    {
        return $this->addData([
            'token' => auth()->refresh(),
            'expires_in' => auth()->factory()->getTTL() * 60
        ])->getResponse();
    }
}

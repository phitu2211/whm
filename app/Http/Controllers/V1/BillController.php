<?php

namespace App\Http\Controllers\V1;

use App\Repositories\Bill\IBillRepo;
use App\Transformers\BillTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class BillController extends ApiController
{
    protected $repo;
    protected $transformer;

    public function __construct(
        IBillRepo $repo,
        BillTransformer $transformer
    ) {
        $this->repo = $repo;
        $this->transformer = $transformer;
        $this->displayName = __('db.order');
        $this->boot();
    }

    public function update(BillRequest $request, $bill_id)
    {
        try {
            $param = $request->all();

            $bill = $this->repo->update($bill_id, $param);

            $bill = Fractal::create()
                ->item($bill, $this->transformer)
                ->toArray();

            return $this->updateSuccess()->addData($bill)->getResponse();
        } catch (\Throwable $th) {
            return $this->setException($th)->getResponse();
        }
    }
}

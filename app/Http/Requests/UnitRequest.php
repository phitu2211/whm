<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'shop_id' => 'required|numeric|exists:shops,id'
        ];

        if ($this->isMethod('post')) {
            array_merge($rules, []);
        }

        if ($this->isMethod('put')) {
            array_merge($rules, []);
        }

        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'shop_id' => 'required|numeric|exists:shops,id'
        ];

        if ($this->isMethod('post')) {
            $rules = array_merge($rules, [
                'phone' => 'required|string|unique:customers,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                'email' => 'nullable|email:rfc,dns|unique:customers,email',
            ]);
        }

        if ($this->isMethod('put')) {
            $rules = array_merge($rules, [
                'phone' => 'required|string|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:customers,phone,' . $this->id,
                'email' => 'nullable|email:rfc,dns|unique:customers,email,' . $this->id
            ]);
        }

        return $rules;
    }
}

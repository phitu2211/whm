<?php

namespace App\Http\Requests;

use App\Enums\BaseStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'barcode' => 'required|string',
            'shop_id' => 'required|numeric|exists:shops,id',
            'unit_id' => 'required|numeric|exists:units,id',
            'category_id' => 'nullable|numeric|exists:categories,id',
            'image' => 'nullable|file',
            'discount' => 'nullable|numeric|min:0',
            'original_price' => 'required|numeric|min:0',
            'final_price' => 'required|numeric|min:0',
            'quantity' => 'required|numeric|min:0',
            'favorite' => 'nullable|boolean',
            'status' => ['nullable', Rule::in(BaseStatus::KEYS)]
        ];

        if ($this->isMethod('post')) {
            $rules = array_merge($rules, []);
        }

        if ($this->isMethod('put')) {
            $rules = array_merge($rules, []);
        }

        return $rules;
    }
}

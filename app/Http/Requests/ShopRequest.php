<?php

namespace App\Http\Requests;

use App\Enums\BaseStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ShopRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'address' => 'required|string',
            'avatar' => 'nullable|file',
            'status' => ['nullable', Rule::in(BaseStatus::KEYS)]
        ];

        if ($this->isMethod('post')) {
            $rules = array_merge($rules, [
                'phone' => 'required|string|unique:shops,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            ]);
        }

        if ($this->isMethod('put')) {
            $rules = array_merge($rules, [
                'phone' => 'required|string|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:shops,phone,' . $this->id,
            ]);
        }

        return $rules;
    }
}

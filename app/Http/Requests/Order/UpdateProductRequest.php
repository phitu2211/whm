<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'product_id' => 'required|numeric|exists:products,id',
            'shop_id' => 'required|numeric|exists:shops,id',
            'quantity' => 'required|numeric|min:1',
        ];

        return $rules;
    }
}

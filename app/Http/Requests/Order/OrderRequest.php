<?php

namespace App\Http\Requests\Order;

use App\Enums\TypeDiscount;
use App\Enums\TypeTax;
use App\Rules\MaxIf;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'discount' => [
                'numeric',
                'min:0',
                new MaxIf(function () {
                    return $this->post('type_discount') == TypeDiscount::PERCENT;
                }, 100)
            ],
            'type_discount' => 'string|in:' . rule_in(TypeDiscount::class),
            'tax' => [
                'numeric',
                'min:0',
                'max:100'
            ],
            'type_tax' => 'string|in:' . rule_in(TypeTax::class),
        ];

        return $rules;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required',
            'password' => 'required',
        ];

        if ($this->isMethod('post')) {
            array_merge($rules, []);
        }

        if ($this->isMethod('put')) {
            array_merge($rules, []);
        }

        return $rules;
    }
}

<?php

namespace App\Exceptions;

class OutOfStockException extends BaseException
{
    public function __construct()
    {
        parent::__construct();
        $this->message = __('exception.out_of_stock');
        $this->code = "ER001";
        $this->statusCode = 200;
    }
}

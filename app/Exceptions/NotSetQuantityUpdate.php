<?php

namespace App\Exceptions;

class NotSetQuantityUpdate extends BaseException
{
    public function __construct()
    {
        parent::__construct();
        $this->message = __('exception.not_set_quantity_update');
        $this->code = "ER003";
        $this->statusCode = 500;
    }
}

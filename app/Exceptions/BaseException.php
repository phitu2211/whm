<?php

namespace App\Exceptions;

use Exception;

class BaseException extends Exception
{
    protected $statusCode = 500;
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}

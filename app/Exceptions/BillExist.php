<?php

namespace App\Exceptions;

class BillExist extends BaseException
{
    public function __construct()
    {
        parent::__construct();
        $this->message = __('exception.bill_exist');
        $this->code = "ER005";
        $this->statusCode = 200;
    }
}

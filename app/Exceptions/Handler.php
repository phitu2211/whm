<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            if ($request->wantsJson()) {
                return $this->handleApiException($request, $e);
            }
        });

        $this->reportable(function (Throwable $e) {
        });
    }

    private function handleApiException($request, Throwable $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
            $exception = $exception->getResponse();
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }

        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {
        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }

        $response = [
            'error' => true,
            'code' => method_exists($exception, 'getCode') ? $exception->getCode() : $statusCode,
            'message' => "Đã có lỗi xảy ra! Vui lòng thử lại!",
        ];

        switch ($statusCode) {
            case 401:
                $response['debug']['message'] = 'Unauthorized';
                $response['message'] = 'Bạn không có quyền sử dụng dịch vụ này! Xin hãy nâng cấp tài khoản!';
                break;
            case 403:
                $response['debug']['message'] = 'Forbidden';
                break;
            case 404:
                $response['debug']['message'] = 'Not Found';
                break;
            case 405:
                $response['debug']['message'] = 'Method Not Allowed';
                break;
            case 422:
                $response['debug']['message'] = $exception->original['message'];
                $response['debug']['errors'] = $exception->original['errors'];
                break;
            default:
                $response['debug']['message'] = ($statusCode == 500) ? "Đã có lỗi xảy ra! Vui lòng thử lại!" : $exception->getMessage();
                break;
        }

        if ($exception instanceof Throwable) {
            $response['debug'] = $this->convertExceptionToArray($exception);
        }

        if (!config('app.debug'))
            unset($response['debug']);

        return response()->json($response, $statusCode);
    }
}

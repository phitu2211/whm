<?php

namespace App\Exceptions;

class NotEnoughMoney extends BaseException
{
    public function __construct()
    {
        parent::__construct();
        $this->message = __('exception.not_enough_money');
        $this->code = "ER004";
        $this->statusCode = 200;
    }
}

<?php

namespace App\Exceptions;

class WrongTypeUpdate extends BaseException
{
    public function __construct()
    {
        parent::__construct();
        $this->message = __('exception.wrong_type_update');
        $this->code = "ER002";
        $this->statusCode = 500;
    }
}

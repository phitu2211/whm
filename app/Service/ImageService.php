<?php

namespace App\Service;

use App\Enums\ImageDrive;
use App\Repositories\Media\IMediaRepo;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    protected $mediaRepo;

    public function __construct(IMediaRepo $mediaRepo)
    {
        $this->mediaRepo = $mediaRepo;
    }

    public function create($image)
    {
        $extension = $image->getClientOriginalExtension() ? $image->getClientOriginalExtension() : "jpg";
        $fileName = gen_file_name_with_extension($extension);
        $path = Storage::disk(ImageDrive::S3)->putFileAs('', $image, $fileName);
        Storage::disk(ImageDrive::S3)->setVisibility($path, 'public');
        $image = $this->mediaRepo->getModel();
        $image->path = $path;
        $image->drive = ImageDrive::S3;

        return $image;
    }
}

<?php

namespace App\Service;

use App\Exceptions\BillExist;
use App\Exceptions\NotEnoughMoney;
use App\Models\Order;
use App\Repositories\Bill\IBillRepo;
use Illuminate\Support\Facades\DB;

class BillService
{
    protected $repo;

    public function __construct(IBillRepo $repo)
    {
        $this->repo = $repo;
    }

    public function createByOrder(Order $order, $customer_pay)
    {
        return DB::transaction(function () use ($order, $customer_pay) {
            if ($customer_pay < $order->total_amount)
                throw new NotEnoughMoney();

            $bill = $this->repo->findByCode($order->code);

            if ($bill)
                throw new BillExist();

            $bill = $this->repo->create([
                'customer_id' => $order->customer_id,
                'shop_id' => $order->shop_id,
                'code' => $order->code,
                'discount_amount' => $order->discount_amount,
                'tax_amount' => $order->tax_amount,
                'customer_pay' => $customer_pay,
                'repay_amount' => $customer_pay - $order->total_amount,
                'total_amount' => $order->total_amount
            ]);

            foreach ($order->products as $product) {
                $bill->products()->save($product, ['quantity' => $product->pivot->quantity]);
            }

            $order->delete();

            return $bill;
        });
    }
}

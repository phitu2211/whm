<?php

namespace App\Service;

use App\Repositories\Category\ICategoryRepo;
use Illuminate\Support\Facades\DB;

class CategoryService
{
    protected $cateRepo;

    public function __construct(ICategoryRepo $cateRepo)
    {
        $this->cateRepo = $cateRepo;
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $cate = $this->cateRepo->find($id);
            foreach ($cate->children as $child) {
                $this->delete($child->id);
            }
            return $cate->delete($id);
        });
    }
}

<?php

namespace App\Service;

use App\Repositories\Media\IMediaRepo;
use App\Repositories\Shop\IShopRepo;
use Illuminate\Support\Facades\DB;

class ShopService
{
    protected $shopRepo;
    protected $imgService;

    public function __construct(IShopRepo $shopRepo, ImageService $imgService)
    {
        $this->shopRepo = $shopRepo;
        $this->imgService = $imgService;
    }

    public function create(array $param, $is_object = true)
    {
        return DB::transaction(function () use ($param, $is_object) {
            $shop = $this->shopRepo->create($param);

            if (key_exists('avatar', $param) && !empty($param['avatar'])) {
                $img = $this->imgService->create($param['avatar']);
                $shop->media()->save($img);
            }
            auth()->user()->shops()->save($shop);

            return $is_object ? $shop : true;
        });
    }

    public function update(array $param, $id, $is_object = true)
    {
        return DB::transaction(function () use ($param, $id, $is_object) {
            $shop = $this->shopRepo->update($id, $param);

            if (!key_exists('avatar', $param) || empty($param['avatar']))
                return $is_object ? $shop : true;

            if ($shop->avatar) {
                $shop->media()->update($param['avatar']);
                return $is_object ? $shop : true;
            }

            $img = $this->imgService->create($param['avatar']);
            $shop->media()->create($img);

            return $is_object ? $shop : true;
        });
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $shop = $this->shopRepo->find($id);
            $shop->media()->delete();
            return $shop->delete($id);
        });
    }
}

<?php

namespace App\Service;

use App\Enums\TypeUpdateQuantity;
use App\Exceptions\OutOfStockException;
use App\Models\Order;
use App\Repositories\Order\IOrderRepo;
use App\Repositories\Product\IProductRepo;
use App\Repositories\Shop\IShopRepo;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderService
{
    protected $repo;
    protected $shopRepo;
    protected $productRepo;
    protected $typeUpdate;
    protected $quantityUpdate;

    public function __construct(IOrderRepo $repo, IShopRepo $shopRepo, IProductRepo $productRepo)
    {
        $this->repo = $repo;
        $this->shopRepo = $shopRepo;
        $this->productRepo = $productRepo;
        $this->typeUpdate = TypeUpdateQuantity::INCREASE;
    }

    public function getOrderByShopId(int $shop_id)
    {
        $shop = $this->shopRepo->find($shop_id);

        $filter = [
            ['shop_id', '=', $shop_id],
            ['user_id', '=', auth()->user()->id]
        ];
        $order = $this->repo->firstWhere($filter);

        if (!$order)
            $order = $this->createOrder($shop);

        return $order;
    }

    public function createOrder($shop)
    {
        $order = $this->repo->create([
            'user_id' => auth()->user()->id,
            'shop_id' => $shop->id,
            'code' => $this->genCode()
        ]);
        return $order;
    }

    private function genCode()
    {
        return "DH" . Carbon::now()->format("dmy.") . time() . auth()->user()->id;
    }

    public function updateProductInOrder(int $product_id, int $shop_id, int $quantity)
    {
        DB::transaction(function () use ($product_id, $shop_id, $quantity) {
            $product = $this->productRepo->find($product_id);
            if (!$product->hasStock())
                throw new OutOfStockException();

            $order = $this->getOrderByShopId($shop_id);
            $this->repo->setModel($order);
            $this->productRepo->setModel($product);

            $productInOrder = $this->repo->findOrCreateProduct($product);

            //Update số lượng sản phẩm trong kho
            $this->productRepo->updateQuantity($product->quantity + $productInOrder->pivot->quantity - $quantity);
            //Update số lượng sản phẩm trong giỏ
            $this->repo->updateQuantityProductInOrder($productInOrder, $quantity);
        });
    }

    public function updateQuantityProductInOrder(int $product_id, int $shop_id)
    {
        if (empty($this->quantityUpdate))
            throw new NotSetQuantityUpdate();

        DB::transaction(function () use ($product_id, $shop_id) {
            $product = $this->productRepo->find($product_id);
            if (!$product->hasStock($this->quantityUpdate))
                throw new OutOfStockException();

            $order = $this->getOrderByShopId($shop_id);
            $this->repo->setModel($order);
            $this->productRepo->setModel($product);

            $productInOrder = $this->repo->findOrCreateProduct($product);

            switch ($this->typeUpdate) {
                case TypeUpdateQuantity::DECREASE:
                    //Update số lượng sản phẩm trong giỏ
                    $this->repo->updateQuantityProductInOrder($productInOrder, $productInOrder->pivot->quantity - $this->quantityUpdate);
                    //Update số lượng sản phẩm trong kho
                    $this->productRepo->updateQuantity($product->quantity + $this->quantityUpdate);
                    break;
                default:
                    //Update số lượng sản phẩm trong giỏ
                    $this->repo->updateQuantityProductInOrder($productInOrder, $productInOrder->pivot->quantity + $this->quantityUpdate);
                    //Update số lượng sản phẩm trong kho
                    $this->productRepo->updateQuantity($product->quantity - $this->quantityUpdate);
                    break;
            }
        });
    }

    public function setTypeUpdate($type)
    {
        if (!in_array($type, TypeUpdateQuantity::getValues()))
            throw new WrongTypeUpdate();

        $this->typeUpdate = $type;
        return $this;
    }

    public function setQuantityUpdate($quantity)
    {
        $this->quantityUpdate = $quantity;
        return $this;
    }
}

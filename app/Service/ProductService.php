<?php

namespace App\Service;

use App\Repositories\Product\IProductRepo;
use Illuminate\Support\Facades\DB;

class ProductService
{
    protected $productRepo;
    protected $imgService;

    public function __construct(IProductRepo $productRepo, ImageService $imgService)
    {
        $this->productRepo = $productRepo;
        $this->imgService = $imgService;
    }

    public function create(array $param, $is_object = true)
    {
        return DB::transaction(function () use ($param, $is_object) {
            $product = $this->productRepo->create($param);

            if (key_exists('image', $param) && !empty($param['image'])) {
                $img = $this->imgService->create($param['image']);
                $product->media()->save($img);
            }

            return $is_object ? $product : true;
        });
    }

    public function update(array $param, $id, $is_object = true)
    {
        return DB::transaction(function () use ($param, $id, $is_object) {
            $img = key_exists('image', $param) ? $param['image'] : null;
            $product = $this->productRepo->update($id, $param);

            if (!$img)
                return $is_object ? $product : true;

            $img = $this->imgService->create($img);
            if ($product->media) {
                $product->media()->update(["path" => $img->path]);
                return $is_object ? $product : true;
            }

            $product->media()->save($img);

            return $is_object ? $product : true;
        });
    }

    public function delete($id)
    {
        return DB::transaction(function () use ($id) {
            $product = $this->productRepo->find($id);
            $product->media()->delete();
            return $product->delete($id);
        });
    }
}

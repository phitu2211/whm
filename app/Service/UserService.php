<?php

namespace App\Service;

use App\Enums\SocialDriver;
use App\Repositories\User\IUserRepo;
use Laravel\Socialite\Facades\Socialite;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserService
{
    /** @var IUserRepo $userRepo */
    protected $userRepo;

    public function __construct(IUserRepo $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function createUserByTokenFb(string $token)
    {
        $user = Socialite::driver(SocialDriver::FB)->userFromToken($token);
        $email = $user->getEmail();
        $name = $user->getName();

        $user = $this->userRepo->getUserByEmail($email);
        if (!$user)
            $user = $this->userRepo->create([
                'name' => $name,
                'email' => $email,
            ]);

        return $user;
    }

    public function updateToken(string $token)
    {
        if (auth()->user()->token) {
            JWTAuth::setToken(auth()->user()->token);
            invalidate_token();
        }

        auth()->user()->update(['token' => $token]);
    }
}

<?php

namespace App\Transformers;

use App\Models\Shop;
use League\Fractal\TransformerAbstract;

class ShopTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Shop $shop)
    {
        return [
            'id' => $shop->id,
            'name' => $shop->name,
            'avatar' => $shop->avatar,
            'phone' => $shop->phone,
            'address' => $shop->address,
            'website' => $shop->website,
            'status' => $shop->statusText
        ];
    }
}

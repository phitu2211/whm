<?php

namespace App\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        if ($category->isParent()) {
            $item = [
                'id'       => $category->id,
                'name'     => $category->name,
                'status' => $category->statusText
            ];

            foreach ($category->children as $key => $value) {
                $item['children'][] = $this->transform($value);
            }

            return $item;
        } else {
            return [
                'id'   => $category->id,
                'name' => $category->name,
                'status' => $category->statusText
            ];
        }
    }
}

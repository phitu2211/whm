<?php

namespace App\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'unit',
        'shop',
        'category'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'barcode' => $product->barcode,
            'original_price' => $product->original_price,
            'discount' => $product->discount,
            'final_price' => $product->final_price,
            'favorite' => $product->favorite,
            'note' => $product->note,
            'status' => $product->statusText,
            'quantity' => $product->quantity,
            'sell_count' => $product->sell_count,
            'image' => $product->image
        ];
    }

    public function includeUnit(Product $product)
    {
        return $this->item($product->unit, new UnitTransformer());
    }

    public function includeShop(Product $product)
    {
        return $this->item($product->shop, app(ShopTransformer::class));
    }

    public function includeCategory(Product $product)
    {
        if (!$product->category)
            return null;
        return $this->item($product->category, app(CategoryTransformer::class));
    }
}

<?php

namespace App\Transformers;

use App\Enums\TypeTax;
use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'id' => $order->id,
            'code' => $order->code,
            'discount' => $order->discount,
            'type_discount' => $order->type_discount,
            'tax' => $order->tax,
            'total_amount' => $order->total_amount,
            'type_tax' => $order->type_tax,
            'tax_info' => [
                'list_type' => TypeTax::TYPE_TAX_OPTIONS,
                TypeTax::BY_PRODUCT => [
                    'amount' => $order->tax_by_product,
                    'total_amount' => $order->total_amount_by_product,
                ],
                TypeTax::BY_ORDER => [
                    'amount' => $order->tax_by_order,
                    'total_amount' => $order->total_amount_by_order,
                ]
            ],
            'customer_name' => $order->customer ? $order->customer->name : 'Vãng lai',
            'products' => $this->products($order)
        ];
    }

    public function products(Order $order)
    {
        if (!$order->products)
            return [];

        return $order->products->map(function ($product) {
            $tax_amount = $product->tax_amount * $product->pivot->quantity;
            $discount_amount = $product->discount_amount * $product->pivot->quantity;
            $total_amount_with_tax = ($product->final_price * $product->pivot->quantity) - $discount_amount + $tax_amount;
            $total_amount_no_tax = ($product->final_price * $product->pivot->quantity) - $discount_amount;

            return [
                'id' => $product->id,
                'name' => $product->name,
                'original_price' => $product->original_price,
                'discount' => $product->discount,
                'type_discount' => $product->type_discount,
                'discount_amount' => $discount_amount,
                'tax' => $product->tax,
                'tax_amount' => $tax_amount,
                'price_each_product' => $product->final_price,
                'total_amount' => [
                    'tax' => $total_amount_with_tax,
                    'no_tax' => $total_amount_no_tax
                ],
                'count' => $product->pivot->quantity,
            ];
        });
    }
}

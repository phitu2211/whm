<?php

namespace App\Transformers;

use App\Models\Bill;
use App\Models\Order;
use League\Fractal\TransformerAbstract;

class BillTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Bill $bill)
    {
        return [
            'id' => $bill->id,
            'code' => $bill->code,
            'customer_name' => $bill->customer ? $bill->customer->name : 'Vãng lai',
            'discount_amount' => $bill->discount_amount,
            'tax_amount' => $bill->tax_amount,
            'customer_pay' => $bill->customer_pay,
            'repay_amount' => $bill->repay_amount,
            'total_amount' => $bill->total_amount,
            'created_at' => $bill->created_at,
            'products' => $this->products($bill)
        ];
    }

    public function products(Bill $bill)
    {
        if (!$bill->products)
            return [];

        return $bill->products->map(function ($product) {
            return [
                'id' => $product->id,
                'name' => $product->name,
                'original_price' => $product->original_price,
                'discount' => $product->discount,
                'final_price' => $product->final_price,
                'count' => $product->pivot->quantity,
            ];
        });
    }
}

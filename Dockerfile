FROM php:7.4-fpm

RUN apt-get update && apt-get install -y libzip-dev

# Extension mysql driver for mysql
RUN docker-php-ext-install pdo_mysql mysqli

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
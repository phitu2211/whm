<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Unit;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::query()->whereName('admin')->count()) {
            $user = User::create([
                'name' => 'admin',
                'phone' => '0123456789',
                'email' => 'admin@admin.com',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('123456'),
            ]);
            $shop = Shop::create([
                'name' => 'Shop Thanh',
                'phone' => '0123456789',
                'address' => 'Cao xá Đức thượng Hoài Đức Hà Nội',
                'website' => "https://hayreview.vn"
            ]);
            $unit = Unit::create([
                'shop_id' => $shop->id,
                'name' => 'Thùng',
            ]);
            $category = Category::create([
                'shop_id' => $shop->id,
                'name' => 'Điện tử'
            ]);
            Product::create([
                'name' => 'Sản phẩm admin',
                'barcode' => 'BarcodeAmin',
                'original_price' => '100000',
                'final_price' => '100000',
                'quantity' => '10',
                'shop_id' => $shop->id,
                'unit_id' => $unit->id,
                'category_id' => $category->id
            ]);
            Customer::create([
                'shop_id' => $shop->id,
                'name' => 'KH Thanh',
                'phone' => '012345679'
            ]);
            $user->shops()->save($shop);

            $user = User::create([
                'name' => 'phitu',
                'phone' => '0352102569',
                'email' => 'phitu@admin.com',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('123456'),
            ]);
            $shop = Shop::create([
                'name' => 'Shop Tu',
                'phone' => '0352102569',
                'address' => 'Cao xá Đức thượng Hoài Đức Hà Nội',
                'website' => "https://hayreview.vn"
            ]);
            $unit = Unit::create([
                'shop_id' => $shop->id,
                'name' => 'Chai',
            ]);
            $category = Category::create([
                'shop_id' => $shop->id,
                'name' => 'Điện lanh'
            ]);
            Product::create([
                'name' => 'Sản phẩm shop Tu',
                'barcode' => 'BarcodeTu',
                'original_price' => '200000',
                'final_price' => '100000',
                'quantity' => '20',
                'shop_id' => $shop->id,
                'unit_id' => $unit->id,
                'category_id' => $category->id
            ]);
            Customer::create([
                'shop_id' => $shop->id,
                'name' => 'KH Tu',
                'phone' => '0352102569'
            ]);
            $user->shops()->save($shop);

            $user = User::create([
                'name' => 'manage',
                'phone' => '9876543210',
                'email' => 'manage@admin.com',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('123456'),
            ]);
            $shop = Shop::create([
                'name' => 'Shop Test',
                'phone' => '9876543210',
                'address' => 'Cao xá Đức thượng Hoài Đức Hà Nội',
                'website' => "https://hayreview.vn"
            ]);
            $unit = Unit::create([
                'shop_id' => $shop->id,
                'name' => 'Hộp',
            ]);
            $category = Category::create([
                'shop_id' => $shop->id,
                'name' => 'Đồ dân dụng'
            ]);
            Product::create([
                'name' => 'Sản phẩm test',
                'barcode' => 'BarcodeAmin',
                'original_price' => '100000',
                'final_price' => '100000',
                'quantity' => '10',
                'shop_id' => $shop->id,
                'unit_id' => $unit->id,
                'category_id' => $category->id
            ]);
            Customer::create([
                'shop_id' => $shop->id,
                'name' => 'KH Test',
                'phone' => '9876543210'
            ]);
            $user->shops()->save($shop);
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->string('name');
            $table->string('barcode');
            $table->string('original_price');
            $table->unsignedInteger('discount')->default(0);
            $table->string('type_discount')->nullable();
            $table->unsignedInteger('tax')->default(0);
            $table->string('final_price');
            $table->unsignedBigInteger('quantity');
            $table->boolean('favorite')->default(0);
            $table->string('note')->nullable();
            $table->unsignedSmallInteger('status')->default(1);
            $table->unsignedBigInteger('sell_count')->default(0);
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('category_id');
            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('id')
                ->on('units')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

<?php

use Illuminate\Support\Facades\Route;

//Auth
Route::get('me', 'AuthController@me')->name('me');
Route::post('login', 'AuthController@login')->withoutMiddleware('auth:api')->name('login');
Route::post('login/fb', 'AuthController@loginFb')->withoutMiddleware('auth:api')->name('login.fb');
Route::post('logout', 'AuthController@logout')->name('logout');
Route::post('refresh', 'AuthController@refresh')->withoutMiddleware('auth:api')->name('refresh');

//Product
Route::group([
    'prefix' => 'product',
    'as' => 'product.'
], function () {
    Route::get('/status', 'ProductController@listStatus')->name('status');
    Route::get('/', 'ProductController@index')->name('list');
    Route::get('/{id}', 'ProductController@detail')->name('detail');
    Route::post('/', 'ProductController@store')->name('store');
    Route::put('/{id}', 'ProductController@update')->name('update');
    Route::delete('/{id}', 'ProductController@destroy')->name('destroy');
});

//Shop
Route::group([
    'prefix' => 'shop',
    'as' => 'shop.'
], function () {
    Route::get('/status', 'ShopController@listStatus')->name('status');
    Route::get('/', 'ShopController@index')->name('list');
    Route::get('/{id}', 'ShopController@detail')->name('detail');
    Route::post('/', 'ShopController@store')->name('store');
    Route::put('/{id}', 'ShopController@update')->name('update');
    Route::delete('/{id}', 'ShopController@destroy')->name('destroy');
});

//Unit
Route::group([
    'prefix' => 'unit',
    'as' => 'unit.'
], function () {
    Route::get('/', 'UnitController@index')->name('list');
    Route::get('/{id}', 'UnitController@detail')->name('detail');
    Route::post('/', 'UnitController@store')->name('store');
    Route::put('/{id}', 'UnitController@update')->name('update');
    Route::delete('/{id}', 'UnitController@destroy')->name('destroy');
});

//User
Route::group([
    'prefix' => 'user',
    'as' => 'user.'
], function () {
    Route::get('/', 'UserController@index')->name('list');
    Route::get('/{id}', 'UserController@detail')->name('detail');
    Route::post('/', 'UserController@store')->name('store');
    Route::put('/{id}', 'UserController@update')->name('update');
    Route::delete('/{id}', 'UserController@destroy')->name('destroy');
});

//Category
Route::group([
    'prefix' => 'category',
    'as' => 'category.'
], function () {
    Route::get('/status', 'CategoryController@listStatus')->name('status');
    Route::get('/', 'CategoryController@index')->name('list');
    Route::get('/{id}', 'CategoryController@detail')->name('detail');
    Route::post('/', 'CategoryController@store')->name('store');
    Route::put('/{id}', 'CategoryController@update')->name('update');
    Route::delete('/{id}', 'CategoryController@destroy')->name('destroy');
});

//Customer
Route::group([
    'prefix' => 'customer',
    'as' => 'customer.'
], function () {
    Route::get('/', 'CustomerController@index')->name('list');
    Route::get('/{id}', 'CustomerController@detail')->name('detail');
    Route::post('/', 'CustomerController@store')->name('store');
    Route::put('/{id}', 'CustomerController@update')->name('update');
    Route::delete('/{id}', 'CustomerController@destroy')->name('destroy');
});

//Order
Route::group([
    'prefix' => 'order',
    'as' => 'order.'
], function () {
    Route::get('/{shop_id}', 'OrderController@index')->name('detail');
    Route::get('confirm/{id}', 'OrderController@confirm')->name('confirm');
    Route::post('/update-product', 'OrderController@updateProduct')->name('update-product');
    Route::post('/update-quantity', 'OrderController@updateQuantity')->name('update-quantity');
    Route::post('/payment', 'OrderController@payment')->name('payment');
    Route::put('/{id}', 'OrderController@update')->name('update');
    Route::delete('/{id}', 'OrderController@destroy')->name('destroy');
});

//Bill
Route::group([
    'prefix' => 'bill',
    'as' => 'bill.'
], function () {
    // Route::put('/{id}', 'BillController@update')->name('update');
});

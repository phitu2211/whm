import Vue from "vue";
import VueX from "vuex";

Vue.use(VueX);

const store = new VueX.Store({
    state: {
        count: 0,
    },
    mutations: {
        INCREMENT(state) {
            state.count++;
        },
    },
    actions: {},
});

export default store;

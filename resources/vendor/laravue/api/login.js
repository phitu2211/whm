import request from "@/utils/request";

export function login(email, password) {
    return request({
        url: "/login",
        method: "post",
        data: {
            email,
            password
        }
    });
}

export function getInfo(token) {
    return request({
        url: "/me",
        method: "get"
    });
}

export function logout() {
    return request({
        url: "/logout",
        method: "post"
    });
}


export function refresh() {
    return request({
        url: "/refresh",
        method: "post"
    });
}

import Cookies from "js-cookie";

const TokenKey = "Admin-Token";

export function getToken() {
    return Cookies.get(TokenKey);
}

export function setToken(token) {
    return Cookies.set(TokenKey, token);
}

export function removeToken() {
    return Cookies.remove(TokenKey);
}

export function hasPermission(roles, permissionRoles) {
    if (roles.indexOf("admin") >= 0) {
        // Admin should have all permissions
        return true;
    }

    if (!permissionRoles) {
        return true;
    }

    return roles.some(role => permissionRoles.indexOf(role) >= 0);
}

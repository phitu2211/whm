import axios from "axios";
import { Message } from "element-ui";
import { getToken, setToken } from "@/utils/auth";

// Create axios instance
const service = axios.create({
    baseURL: process.env.MIX_BASE_API,
    timeout: 10000 // Request timeout
});

service.interceptors.request.use(
    config => {
        const token = getToken();
        if (token) {
            config.headers["Authorization"] = "Bearer " + getToken(); // Set JWT token
        }

        return config;
    },
    error => {
        // Do something with request error
        if (process.env.MIX_APP_DEBUG === "true") console.log(error);
        Promise.reject(error);
    }
);

// response pre-processing
service.interceptors.response.use(
    response => {
        if (response.headers.authorization) {
            setToken(response.headers.authorization);
            response.data.token = response.headers.authorization;
        }

        return response.data;
    },
    error => {
        if (process.env.MIX_APP_DEBUG === "true") {
            console.log(error.response);
            if (error.response.status != 401)
                Message({
                    message: error.response.data.message,
                    type: "error",
                    duration: 5 * 1000
                });
        } else {
            if (error.response.status != 401)
                Message({
                    message: "Đã có lỗi xảy ra",
                    type: "error",
                    duration: 5 * 1000
                });
        }
        return Promise.reject(error);
    }
);

export default service;

<?php

return [
    'shop' => 'cửa hàng',
    'product' => 'sản phẩm',
    'unit' => 'đơn vị',
    'category' => 'danh mục',
    'customer' => 'khách hàng',
    'order' => 'đơn hàng',
    'bill' => 'hóa đơn',
];

<?php

return [
    'create_success' => 'Tạo mới :model thành công',
    'update_success' => 'Cập nhập :model thành công',
    'delete_success' => 'Xóa :model thành công',
];

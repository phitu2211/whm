<?php

return [
    'out_of_stock' => 'Không còn sản phẩm trong kho',
    'wrong_type_update' => 'Sai kiểu cập nhật số lượng sản phẩm',
    'not_enough_money' => 'Không đủ tiền thanh toán',
    'bill_exist' => 'Đơn hàng đang/đã được thanh toán',
    'not_set_quantity_update' => 'Chưa set số lượng sản phẩm cần cập nhật trong đơn hàng'
];

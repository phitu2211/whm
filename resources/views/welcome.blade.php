<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" , content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <title>{{ env('APP_NAME') }}</title>
</head>

<body>
    <div class="" id="app">
        <app></app>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>